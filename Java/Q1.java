import java.util.*;


class BankAccount{
	Scanner input = new Scanner(System.in);
	private int accNumber;
	private float balance;
	protected BankAccount(){
		System.out.println("Enter account no.: ");
		this.accNumber = input.nextInt();
		System.out.println("Enter balance: ");
		this.balance = input.nextFloat();
	}
	protected BankAccount(int accNumber, float balance){
		this.accNumber = accNumber;
		this.balance = balance;

	}
	protected float deposit(float amount){
		this.balance += amount;
		return this.balance;
	}
	protected float withdraw(float amount){
		this.balance -= amount;
		return this.balance;
	}
	protected int getAccountNo(){
		return this.accNumber;
	}
	protected float getBalance(){
		return this.balance;
	}

}

class Bank extends BankAccount{
	private ArrayList<BankAccount> accounts;
	private float totalBankBalance;
	private int maxBalanceAccount;
	private float maxBalance;
	private int minBalanceAccount;
	private float minBalance;
	private int numberOfAccounts = 0;

	public Bank(){
		this.accounts = new ArrayList();
		this.totalBankBalance = 0.0f;
	}

	public void addAccount(){
		BankAccount newAccount = new BankAccount();
		if(accounts.size() == 0){
			this.minBalanceAccount = newAccount.getAccountNo();
			this.minBalance = newAccount.getBalance();
			this.maxBalanceAccount = newAccount.getAccountNo();
			this.maxBalance = newAccount.getBalance();
		}
		else{
			if(newAccount.getBalance() > this.maxBalance){
				this.maxBalance = newAccount.getBalance();
				this.maxBalanceAccount = newAccount.getAccountNo();
			}
			if(newAccount.getBalance() < this.minBalance){
				this.minBalance = newAccount.getBalance();
				this.minBalanceAccount = newAccount.getAccountNo();
			}
		}
		accounts.add(newAccount);
		this.totalBankBalance += newAccount.getBalance();
		numberOfAccounts += 1;
	}
	public float getTotalBankBalance(){
		return this.totalBankBalance;
	}
	public void getMinMax(){
		System.out.println("Account with MAXIMUM Balance: "+this.maxBalanceAccount);
		System.out.println("Account with MINIMUM Balance: "+this.minBalanceAccount);
	}

	public boolean searchAccount(int accNumber){
		Iterator<BankAccount> itr = this.accounts.iterator();
		while(itr.hasNext()){
			if(itr.next().getAccountNo() == accNumber){
				return true;
			}
		}
		return false;
	}

	public void accountsWithBalance(float balance){
		Iterator<BankAccount> itr = this.accounts.iterator();
		int num = 0;
		while(itr.hasNext()){
			if(itr.next().getBalance() >= balance){
				num ++;
			}
		}
		System.out.println(num+" account(s) has/have at least "+balance);
	}
}