import java.util.*;
abstract class Stack{
	public abstract void push(int data);
	public abstract void pop();
	public abstract void display();
}

class StaticStack extends Stack{
	private int[] array;
	private int size;
	private int head = -1;
	public StaticStack(int size){
		this.size = size;
		this.head = 0;
		this.array = new int [size];
		for(int i = 0; i < this.size; i++){
			this.array[i] = '\0';
		}
	}
	public void push(int data){
		if(this.head <= this.size){
			this.array[this.head] = data;
			this.head++;
		}
		else{
			System.out.println("Error! Stack Overflow!");
		}
	}
	public void pop(){
		if(this.head>0){
			this.head--;
			this.array[this.head] = '\0';
		}
		else{
			System.out.println("Error! Stack Underflow!");
		}
	}
	public void display(){
		if(this.head > 0){
			for(int i = 0; i <= this.head; i++){
				System.out.print(this.array[i]+"->");
			}
		}
		else{
			System.out.println("Empty Stack!");
		}
	}
}

class DynamicStack extends Stack{
	private ArrayList stack;
	public DynamicStack(){
		this.stack = new ArrayList<Integer> ();
	}
	public void push(int data){
		this.stack.add(data);
	}
	public void pop(){
		if(this.stack.size() == 0){
			System.out.println("Error! Stack Underflow");
		}
		else{
			this.stack.remove(this.stack.size()-1);
		}
	}
	public void display(){
		Iterator<Integer> itr = stack.iterator();
		while(itr.hasNext()){
			System.out.print(itr.next()+"->");
		}
	}
}