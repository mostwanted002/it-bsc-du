var nameResult = false;
var rollResult = false;
var dobResult = false;
var emptyFields = true;
var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var d = '';
var name = '';
var rollNo = '';
var dob = '';

function verify(){
	name = new String(document.getElementById('name').value);
	rollNo = new String(document.getElementById('rollNo').value);
	dob = new String(document.getElementById('dob').value);
	if(name=='' || rollNo == '' || dob == ''){
		emptyFields = true;
	}
	else{
		emptyFields = false;
	}
	var check = new RegExp("^[0-9]{7}$");
	rollResult = check.test(rollNo);
	checkDob(dob);
	check = /[a-z]/i;
	nameResult = check.test(name);
	result();

}

function checkDob(dob) {
	var patt = new RegExp("^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$");
	if(patt.test(dob)){
		var dobArray = dob.split('/');
		var dd = dobArray[0];
		var mm = dobArray[1];
		var yy = dobArray[2];
		if((dd>0 && dd < 32) && (mm >= 0 && mm < 12) && (yy > 0 && yy <= 99)){
			d = new Date(new String(dobArray[2]), dobArray[1]-1, dobArray[0]);
		}
		dobResult = true;
	}
	else{
		dobResult= false;
	}
}

function result() {
	if (nameResult && rollResult && dobResult && !emptyFields){
		rs = 'Registration Sucessful!</br>Name:'+name+'</br>RollNo: '+rollNo+'</br>Date of Birth: '+days[d.getDay()]+', '+months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear();
		document.getElementById('result').innerHTML = rs;
	//	console.log(rs);
	}
	else{
		rs = 'Error! Check the information you\'ve entered.';
		document.getElementById('result').innerHTML = rs;
		rs = name+' '+rollNo+' '+days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear();
//		console.log(rs);
//		console.log(nameResult + rollResult + dobResult + emptyFields);
	}
}