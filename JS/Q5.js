var cols = new Array('voilet', 'indigo', 'blue', 'green', 'yellow', 'orange', 'red', '#212121', '#313131', '#deadbe');
var textAnim;
var imgAnim;
var pos = new Array('10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%');


function strtClrChg(){
	textAnim = setTimeout(function(){
		var rnd = Math.floor(Math.random() * 10);
		document.getElementById('text1').style.color = cols[rnd];
	}, 1000);
}

function stpClrChg(){
	clearTimeout(textAnim);
}


function strtMvChg(){
	imgAnim = setInterval(function(){
		var rnd = Math.floor(Math.random() * 10);  
		document.getElementById('image1').style.position = 'absolute';
		document.getElementById('image1').style.left = pos[rnd];
	}, 1000);
}

function stpMvChg(){
	clearInterval(imgAnim);
}