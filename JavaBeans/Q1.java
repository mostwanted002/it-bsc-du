import java.io.*;

class StudentBean implements Serializable{
	private String firstName;
	private String lastName;
	private int rollNo;

	public String getName(){
		return(firstName+lastName);
	}
	public void setName(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public int getRollNo(){
		return rollNo;
	}
	public void setRollNo(int rollNo){
		this.rollNo = rollNo;
	}
}