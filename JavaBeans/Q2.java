import java.io.*;

class Employee implements Serializable{
	private String firstName;
	private String lastName;
	private String department;
	private String location;
	private float salary;

	public String getName(){
		return(this.firstName+this.lastName);
	}
	public void setName(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public float getSalary(){
		return this.salary;
	}
	public void setSalary(float salary){
		this.salary = salary;
	}
	public String getDept(){
		return this.department;
	}
	public void setDept(String department){
		this.department = department;
	}
	public String getLocation(){
		return this.department;
	}
	public void setLocation(String department){
		this.department = department;
	}
}