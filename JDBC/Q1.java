import java.sql.*;
import java.util.*;


public class Q1{
    static final String username = "mysql";
    static final String password = "passw0rd";

    public static void main(String[] args){
    	Connection conn = null;
    	Statement stmt = null;
    	Scanner input = new Scanner(System.in);
    	try{
	    	System.out.println("Connecting to database...");
			conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/practicals?user=mysql&password=passw0rd");
    		System.out.println("Creating Statement No. 1..");
        	stmt = conn.createStatement();
  	        String sql = "select count(rollNo) as totalStudents from studentInfo;";
        	ResultSet rs = stmt.executeQuery(sql);
        	if(rs.next()){
        		System.out.println("Total No. of students :"+rs.getInt("totalStudents"));
        	}
        	rs.close();
        	sql = "select avg(marksEng) as 'avgEng', avg(marksMaths) as 'avgMath', avg(marksSci) as 'avgSci', avg(marksSST) as 'avgSST', avg(marksHin) as 'avgHin' from studentInfo natural left join result;";
        	rs = stmt.executeQuery(sql);
        	if(rs.next()){
        		System.out.println("Avg. Marks in English :"+rs.getFloat("avgEng"));
        		System.out.println("Avg. Marks in Maths :"+rs.getFloat("avgMath"));
        		System.out.println("Avg. Marks in Science :"+rs.getFloat("avgSci"));
        		System.out.println("Avg. Marks in Social Studies :"+rs.getFloat("avgSST"));
        		System.out.println("Avg. Marks in Hindi :"+rs.getFloat("avgHin"));

        	}
        	rs.close();
        	sql = "";
    	}catch (Exception e){
	    	System.out.println("ehh...., something went wrong. :(");
    		e.printStackTrace();
    	}
	}
}